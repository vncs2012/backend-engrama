from rest_framework.viewsets import ModelViewSet
from . import models
from . import serializers
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
from Core.permissions import IsLoggedInUserOrAdmin, IsAdminUser
from django.http import JsonResponse, HttpResponse
from django.middleware.csrf import get_token


@permission_classes((AllowAny, ))
class PerguntaQuestionarioViewset(ModelViewSet):
    queryset = models.PerguntaQuestionario.objects.all()
    serializer_class = serializers.PerguntaQuestionarioSerializer
    
@permission_classes((AllowAny, ))
class EneaTipoViewset(ModelViewSet):
    queryset = models.EneaTipo.objects.all()
    serializer_class = serializers.EneaTipoSerializer
    
@permission_classes((AllowAny, ))
class CaracteristicasPersonaViewset(ModelViewSet):
    queryset = models.CaracteristicasPersona.objects.all()
    serializer_class = serializers.CaracteristicasPersonaSerializer

@permission_classes((AllowAny, ))
class ParticipantesViewset(ModelViewSet):
    queryset = models.Participantes.objects.all()
    serializer_class = serializers.ParticipantesSerializer

@permission_classes((AllowAny, ))
class UserViewSet(ModelViewSet):
    queryset = models.User.objects.all()
    serializer_class = serializers.UserSerializer
    def get_permissions(self):
        permission_classes = []
        if self.action == 'create':
            permission_classes = [AllowAny]
        elif self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
            permission_classes = [IsLoggedInUserOrAdmin]
        elif self.action == 'list' or self.action == 'destroy':
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

def csrf(request):
    print(get_token(request))
    return JsonResponse({'csrfToken': get_token(request)})

@permission_classes((AllowAny, ))
def ping(request):
    return JsonResponse({'result': 'OK'})