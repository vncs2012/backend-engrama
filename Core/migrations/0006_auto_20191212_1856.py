# Generated by Django 2.2.6 on 2019-12-13 00:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Core', '0005_auto_20191009_2346'),
    ]

    operations = [
        migrations.CreateModel(
            name='Participantes',
            fields=[
                ('cd_participante', models.IntegerField(editable=False, primary_key=True, serialize=False)),
                ('no_participante', models.CharField(blank=True, max_length=130, verbose_name='Nome ')),
                ('nu_cpf', models.CharField(blank=True, max_length=11, null=True, verbose_name='CPF ')),
                ('Tel_numero', models.CharField(blank=True, max_length=14, verbose_name='Telefone')),
            ],
        ),
        migrations.AlterField(
            model_name='caracteristicaspersona',
            name='tp_caracteristica',
            field=models.IntegerField(choices=[(None, ''), (1, 'PERFECCIONISTA'), (2, 'PRESTATIVO'), (3, 'BEM-SUCEDIDO'), (4, 'ROMÂNTICO'), (5, 'OBSERVADOR'), (6, 'QUESTIONADOR'), (7, 'SONHADOR'), (8, 'CONFRONTADOR'), (9, 'PRESERVACIONISTA')], verbose_name='Tipo : '),
        ),
        migrations.AlterField(
            model_name='caracteristicaspersona',
            name='tp_predominate',
            field=models.IntegerField(choices=[(None, ''), (1, 'PADRÃO'), (2, 'PREDOMINANTE')], verbose_name='Predominante? : '),
        ),
    ]
