from django.urls import path, include
from rest_framework import routers
from Core import views as myapp_views

router = routers.DefaultRouter()
router.register(r'perguntas', myapp_views.PerguntaQuestionarioViewset)
router.register(r'eneatipo', myapp_views.EneaTipoViewset)
router.register(r'caracteristica', myapp_views.CaracteristicasPersonaViewset)
router.register(r'participantes', myapp_views.ParticipantesViewset)
router.register(r'users', myapp_views.UserViewSet)

urlpatterns = [
    path(r'csrf', myapp_views.csrf),
    path(r'ping', myapp_views.ping),
]

urlpatterns += router.urls