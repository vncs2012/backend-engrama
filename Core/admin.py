from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import User, UserProfile, PerguntaQuestionario


class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False

# @admin.register(User)
class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'first_name', 'last_name', 'is_staff')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)
    inlines = (UserProfileInline, )


# @admin.register(PerguntaQuestionario)
class PerguntaQuestionarioAdmin(admin.ModelAdmin):
    fields = ['nu_pergunta', 'ds_pergunta','res_per_questionarioA','val_per_questionarioA','res_per_questionarioB','val_per_questionarioB']
    list_display = ('nu_pergunta', 'ds_pergunta')
    search_fields = ('nu_pergunta', 'ds_pergunta')
    ordering = ('nu_pergunta',)

admin.site.register(User, UserAdmin)
admin.site.register(PerguntaQuestionario, PerguntaQuestionarioAdmin)
